package com.utils;

import com.typesafe.config.ConfigFactory;

public class Config implements Configuration {

	final com.typesafe.config.Config config = ConfigFactory.load();


	public String getEnvironment() {

		return config.getString("env").toString();
	}

	public String getHomePage() {
		final String defaultUrl = System.getProperty("defaultUrl") != null ? System.getProperty("defaultUrl"): config.getString("defaultUrl").toString();

		return defaultUrl;
	}

}
