package com.selenium.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class CustomExpectedCondition implements ExpectedCondition {
	WebDriver d;
	int expectedWindows;
	By parentLocator;
	By resultLocator;


	public CustomExpectedCondition(By resultLocator, By parentLocator, int expectedWindows, WebDriver d) {
		this.resultLocator = resultLocator;
		this.parentLocator = parentLocator;
		this.d = d;
		this.expectedWindows = expectedWindows;
	}


	public Object apply(Object input) {

		return d.getWindowHandles().size()==expectedWindows;
	}


}
