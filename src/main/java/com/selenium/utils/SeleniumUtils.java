package com.selenium.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;




public class SeleniumUtils extends DriverImpl {

	static WebDriver localDriver;
	final static Logger LOG = Logger.getLogger(SeleniumUtils.class);

	public static void clearSessionStorage() {
		((JavascriptExecutor) localDriver).executeScript("window.localStorage.clear()");
		((JavascriptExecutor) localDriver).executeScript("window.sessionStorage.clear()");
		localDriver.manage().deleteAllCookies();
	}



	public static WebElement getElement(final By selector) {

		return localDriver.findElement(selector);
	}

	public static void log(final String msg) {

		System.out.println(msg);
	}

	public SeleniumUtils(WebDriver driver) {
		localDriver = driver;
	}

	public void clearInput(WebElement e) {
		e.getText().length();
		for(int y = 0; y < 15; y++) {
			e.sendKeys(Keys.BACK_SPACE);
		}

	}
	public void clickElement(String labelElement) {

		labelElement = labelElement.replaceAll("([a-z0-9])([A-Z])", "$1-$2").toLowerCase().replaceAll(" ", "-");
		getWebDriverWait().until(ExpectedConditions.elementToBeClickable(By.className(labelElement)));
		localDriver.findElement(By.className(labelElement)).click();



	}

	public WebElement getElementByText(String text) {
		((JavascriptExecutor) localDriver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[text()='"+text+"']")));
		try {
			Thread.sleep(500);
		} catch (final InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return driver.findElement(By.xpath("//*[text()='"+text+"']"));


	}

	public String getKebabCaseValue(String labelElement) {
		return labelElement.replaceAll("([a-z0-9])([A-Z])", "$1-$2").toLowerCase().replaceAll(" ", "-");
	}

	public String getText(String className) {
		final String selector = className.replaceAll("([a-z0-9])([A-Z])", "$1-$2").toLowerCase().replaceAll(" ", "-");
		getWebDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.className(selector)));

		return localDriver.findElement(By.className(selector)).getText();

	}

	public WebDriverWait getWebDriverWait() {

		return new WebDriverWait(localDriver, 60);
	}
	public void scrollToElement(By selector) {

		((JavascriptExecutor) localDriver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(selector));
		((JavascriptExecutor) localDriver).executeScript("window.scrollBy(0, -100);");

	}

	public void selectOptionByIndex(String labelElement, Integer index) {

		if(index!=null) {

			labelElement = labelElement.replaceAll("([a-z0-9])([A-Z])", "$1-$2").toLowerCase().replaceAll(" ", "-");
			getWebDriverWait().until(ExpectedConditions.elementToBeClickable(By.className(labelElement)));


			localDriver.findElement(By.className(labelElement)).click();
			getWebDriverWait().until(ExpectedConditions.elementToBeClickable(By.className("mat-option")));


			localDriver.findElements(By.className("mat-option")).get(index).click();
			//getDriver().findElement(By.className(labelElement)).sendKeys(Keys.TAB);

		}
	}

	public void sendData(String labelElement, final String data) {

		if(data!=null) {

			labelElement = labelElement.replaceAll("([a-z0-9])([A-Z])", "$1-$2").toLowerCase().replaceAll(" ", "-");
			getWebDriverWait().until(ExpectedConditions.elementToBeClickable(By.className(labelElement)));
			localDriver.findElement(By.className(labelElement)).click();
			clearInput(localDriver.findElement(By.className(labelElement)));
			localDriver.findElement(By.className(labelElement)).sendKeys(String.valueOf(data));
			localDriver.findElement(By.className(labelElement)).sendKeys(Keys.TAB);



		}

	}


	public void sendDataBy(WebDriver driver, By selector, final String data) {

		if(data!=null) {
			try {
				getWebDriverWait().until(ExpectedConditions.elementToBeClickable(selector));
				localDriver.findElement(selector).click();
				localDriver.findElement(selector).clear();
				localDriver.findElement(selector).sendKeys(data);
				localDriver.findElement(selector).sendKeys(Keys.TAB);


			}catch(final Exception e) {

				LOG.error("Unable to send data to the element:"+selector);
				LOG.error(e.getMessage());
			}
		}

	}




	public void switchToNewWindow() {
		localDriver.getWindowHandle();


		for(final String winHandle : localDriver.getWindowHandles()){

			localDriver.switchTo().window(winHandle);

		}
	}



	public void waitForElement(WebDriver driver, WebElement element) {
		try {
			getWebDriverWait().until(ExpectedConditions.visibilityOf(element));
		}catch(final Exception e) {
			LOG.error("Element is not displayed:"+element);
		}
	}

	public void waitForElementWithText(String text) {
		try {

			getWebDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[text()='"+text+"']")));
		}catch(final Exception e) {
			LOG.error("Element with text is not displayed:"+text);
		}
	}

	public void waitForPage(String selector) {

		selector = selector.replaceAll("([a-z0-9])([A-Z])", "$1-$2").toLowerCase().replaceAll(" ", "-");
		getWebDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.className(selector)));

	}
}
