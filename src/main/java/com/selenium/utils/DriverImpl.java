package com.selenium.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.SessionId;

import com.utils.Config;

public class DriverImpl extends Config implements DriverContract {

	public static WebDriver driver;

	static SessionId session;


	/**
	 * @param headless
	 * @return
	 */
	public static WebDriver driver(final Boolean headless) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\GUEDUA\\Data\\Confidential\\Common\\chromedriver_win32\\chromedriver.exe");

		WebDriver localDriver;
		if (headless) {
			final ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200",
					"--ignore-certificate-errors");
			localDriver = new ChromeDriver(options);
			session = ((ChromeDriver)driver).getSessionId();

		} else {
			localDriver = new ChromeDriver();

		}
		return localDriver;

	}

	Boolean headless = System.getProperty("headless") != null ? Boolean.parseBoolean(System.getProperty("headless"))
			: false;

	public WebDriver getDriver() {


		if (driver == null) {

			setDriver(headless);
		}
		if(((ChromeDriver)driver).getSessionId()==null) {
			setDriver(headless);
		}

		return driver;
	}



	/**
	 * @param headless
	 */
	public void setDriver(final Boolean headless) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\GUEDUA\\Data\\Confidential\\Common\\chromedriver_win32\\chromedriver.exe");


		if (headless) {
			final ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200",
					"--ignore-certificate-errors");
			driver = new ChromeDriver(options);
			session = ((ChromeDriver)driver).getSessionId();

		} else {
			driver = new ChromeDriver();

		}
		driver.manage().window().maximize();

	}

}
