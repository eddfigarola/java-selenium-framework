package com.selenium.utils;

import org.openqa.selenium.WebDriver;

public interface DriverContract {

	WebDriver getDriver();

	void setDriver(Boolean headless);
}
