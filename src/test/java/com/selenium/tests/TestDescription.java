package com.selenium.tests;

public @interface TestDescription {

	String value();
}
